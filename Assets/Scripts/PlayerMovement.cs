﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D playerRigidbody;
    public float moveSpeed = 1f;

    public void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
    }


    void FixedUpdate()
    {
        if (playerRigidbody != null)
        {
            ApplyInput();

        }
        else
        {
            Debug.LogWarning("Rigid body not attached to player" + gameObject.name);
        }
    }

    public void ApplyInput()
    {
        float xInput = Input.GetAxis("Horizontal");
        float yInput = Input.GetAxis("Vertical");

        float xForce = xInput * moveSpeed * Time.deltaTime;

        Vector2 force = new Vector2(xForce, 0);

        playerRigidbody.AddForce(force);

        
    }
}
